---
backup:
    files:
    - /etc/network/interfaces
    - /etc/frr/frr.conf
    - /etc/frr/daemons
    path: ../inventories/evpn_mh/config/border02
bgp:
    address_family:
    -   name: ipv4_unicast
        redistribute:
        -   type: connected
    -   advertise_all_vni: true
        name: l2vpn_evpn
        neighbors:
        -   activate: true
            interface: underlay
    asn: '65164'
    neighbors:
    -   interface: rp_link
        peergroup: underlay
        unnumbered: true
    -   interface: swp51
        peergroup: underlay
        unnumbered: true
    -   interface: swp52
        peergroup: underlay
        unnumbered: true
    -   interface: swp53
        peergroup: underlay
        unnumbered: true
    -   interface: swp54
        peergroup: underlay
        unnumbered: true
    peergroups:
    -   name: underlay
        remote_as: external
    router_id: 10.10.10.64
    vrfs:
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: static
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: RED
        router_id: 10.10.10.64
    -   address_family:
        -   name: ipv4_unicast
            redistribute:
            -   type: static
        -   extras:
            - advertise ipv4 unicast
            name: l2vpn_evpn
        name: BLUE
        router_id: 10.10.10.64
bgp_asn_prefix: 651
bonds:
-   name: rp_link
    options:
        mtu: 9216
        pim: true
    ports:
    - swp49
    - swp50
-   bridge:
        vids:
        - 101
        - 102
    es_df_pref: '1'
    es_id: 1
    name: bond1
    options:
        mtu: 9000
    ports:
    - swp3
bridge:
    vids:
    - 101
    - 102
    - 4001
    - 4002
discovered_interpreter_python: /usr/bin/python
dns:
    domain: cumulusnetworks.com
    search_domain:
    - cumulusnetworks.com
    servers:
        ipv4:
        - 1.1.1.1
        - 8.8.8.8
        vrf: mgmt
es_df_pref: 1
eth0:
    ips:
    - 192.168.200.64/24
eth0_id: 64
eth0_ip: 192.168.200.64/24
eth0_subnet: 192.168.200.0/24
evpn_mh:
    rp: true
    sysmac: 44:38:39:BE:EF:FF
evpn_mh_sysmac_prefix: 44:38:39:BE:EF
fabric_name: evpn_mh
group_names:
- border
- pod1
groups:
    all:
    - netq-ts
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    border:
    - border01
    - border02
    fw:
    - fw1
    leaf:
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    netq:
    - netq-ts
    pod1:
    - fw1
    - leaf01
    - leaf02
    - leaf03
    - leaf04
    - spine01
    - spine02
    - spine03
    - spine04
    - border01
    - border02
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    server:
    - server01
    - server02
    - server03
    - server04
    - server05
    - server06
    spine:
    - spine01
    - spine02
    - spine03
    - spine04
    ungrouped: []
id: 64
interfaces:
-   name: swp51
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp52
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp53
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
-   name: swp54
    options:
        evpn_mh_uplink: true
        extras:
        - alias to spine
        pim: true
inventory_dir: /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh
inventory_file: /home/cumulus/cumulus_ansible_modules/inventories/evpn_mh/hosts
inventory_hostname: border02
inventory_hostname_short: border02
leaf_spine_interface:
    evpn_mh_uplink: true
    extras:
    - alias to spine
    pim: true
loopback:
    igmp: true
    ips:
    - 10.10.10.64/32
    - 10.10.100.100/32
    pim:
        source: 10.10.10.64
    vxlan_local_tunnel_ip: 10.10.10.64
mac_prefix: 00:00:00:00:00
msdp:
    mesh:
    - border01
    - border02
    source: 10.10.10.64
netq:
    agent_server: 192.168.200.250
    cli_access_key: long-key-0987654321
    cli_port: 443
    cli_premises: CITC
    cli_secret_key: long-key-1234567890
    cli_server: api.air.netq.cumulusnetworks.com
    version: latest
ntp:
    server_ips:
    - 0.cumulusnetworks.pool.ntp.org
    - 1.cumulusnetworks.pool.ntp.org
    - 2.cumulusnetworks.pool.ntp.org
    - 3.cumulusnetworks.pool.ntp.org
    timezone: America/Los_Angeles
pim:
    ecmp: true
    keep_alive: 3600
    rp_mcast_range: 224.0.0.0/4
    rpaddr: 10.10.100.100/32
    source: 10.10.10.64
playbook_dir: /home/cumulus/cumulus_ansible_modules/playbooks
snmp:
    addresses:
    - 192.168.200.64/24@mgmt
    - udp6:[::1]:161
    rocommunity: public
spine_leaf_interface:
    extras:
    - alias to leaf
    pim: true
ssh:
    banner: |-
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Authorized Users Only!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    motd: |-
        #########################################################
        Successfully logged in to: border02
        #########################################################
syslog:
    servers:
    - 192.168.200.1
sysmac: FF
tacacs:
    groups:
    -   name: admins
        priv_level: 15
    -   name: basics
        priv_level: 1
    secret: tacacskey
    server_ips:
    - 192.168.200.1
    users:
    -   group: basics
        name: basicuser
        password: password
    -   group: admins
        name: adminuser
        password: password
    vrf: mgmt
vlan10:
    address:
    - 10.1.10.65/24
    address_virtual:
    - 00:00:00:00:00:10 10.1.10.1/24
    id: 10
    name: vlan10
    vrf: RED
vlan101:
    address:
    - 10.1.101.65/24
    address_virtual:
    - 00:00:00:00:00:01 10.1.101.1/24
    id: 101
    name: vlan101
    vrf: RED
vlan101_subnet: 10.1.101.0/24
vlan102:
    address:
    - 10.1.102.65/24
    address_virtual:
    - 00:00:00:00:00:02 10.1.102.1/24
    id: 102
    name: vlan102
    vrf: BLUE
vlan102_subnet: 10.1.102.0/24
vlan10_subnet: 10.1.10.0/24
vlan20:
    address:
    - 10.1.20.65/24
    address_virtual:
    - 00:00:00:00:00:20 10.1.20.1/24
    id: 20
    name: vlan20
    vrf: RED
vlan20_subnet: 10.1.20.0/24
vlan30:
    address:
    - 10.1.30.65/24
    address_virtual:
    - 00:00:00:00:00:30 10.1.30.1/24
    id: 30
    name: vlan30
    vrf: BLUE
vlan30_subnet: 10.1.30.0/24
vlan4001:
    hwaddress: 44:38:39:BE:EF:FF
    id: 4001
    name: vlan4001
    vrf: RED
vlan4002:
    hwaddress: 44:38:39:BE:EF:FF
    id: 4002
    name: vlan4002
    vrf: BLUE
vlans:
-   address:
    - 10.1.101.65/24
    address_virtual:
    - 00:00:00:00:00:01 10.1.101.1/24
    id: 101
    name: vlan101
    vrf: RED
-   address:
    - 10.1.102.65/24
    address_virtual:
    - 00:00:00:00:00:02 10.1.102.1/24
    id: 102
    name: vlan102
    vrf: BLUE
-   hwaddress: 44:38:39:BE:EF:FF
    id: 4001
    name: vlan4001
    vrf: RED
-   hwaddress: 44:38:39:BE:EF:FF
    id: 4002
    name: vlan4002
    vrf: BLUE
vni10:
    extras:
    - vxlan-mcastgrp 224.0.0.10
    name: vni10
    vlan_id: 10
    vxlan_id: 10
vni101:
    extras:
    - vxlan-mcastgrp 224.0.0.101
    name: vni101
    vlan_id: 101
    vxlan_id: 101
vni102:
    extras:
    - vxlan-mcastgrp 224.0.0.102
    name: vni102
    vlan_id: 102
    vxlan_id: 102
vni20:
    extras:
    - vxlan-mcastgrp 224.0.0.20
    name: vni20
    vlan_id: 20
    vxlan_id: 20
vni30:
    extras:
    - vxlan-mcastgrp 224.0.0.30
    name: vni30
    vlan_id: 30
    vxlan_id: 30
vniBLUE:
    name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vniRED:
    name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
vnis:
-   extras:
    - vxlan-mcastgrp 224.0.0.101
    name: vni101
    vlan_id: 101
    vxlan_id: 101
-   extras:
    - vxlan-mcastgrp 224.0.0.102
    name: vni102
    vlan_id: 102
    vxlan_id: 102
-   name: vniRED
    vlan_id: 4001
    vxlan_id: 4001
-   name: vniBLUE
    vlan_id: 4002
    vxlan_id: 4002
vrf:
-   extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
-   name: RED
    routes:
    - 10.1.30.0/24 10.1.101.4
    vxlan_id: 4001
-   name: BLUE
    routes:
    - 10.1.10.0/24 10.1.102.4
    - 10.1.20.0/24 10.1.102.4
    vxlan_id: 4002
vrf_BLUE:
    name: BLUE
    vxlan_id: 4002
vrf_BLUE_border:
    name: BLUE
    routes:
    - 10.1.10.0/24 10.1.102.4
    - 10.1.20.0/24 10.1.102.4
    vxlan_id: 4002
vrf_RED:
    name: RED
    vxlan_id: 4001
vrf_RED_border:
    name: RED
    routes:
    - 10.1.30.0/24 10.1.101.4
    vxlan_id: 4001
vrf_default_FW:
    name: default
    routes:
    - 10.1.10.0/24 10.1.101.1
    - 10.1.20.0/24 10.1.101.1
    - 10.1.30.0/24 10.1.102.1
vrf_mgmt:
    extras:
    - address 127.0.0.1/8
    - address ::1/128
    name: mgmt
    routes:
    - 0.0.0.0/0 192.168.200.1
vxlan_local_loopback: 10.10.10.64/32
...
